namespace InsuranceT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Employee")]
    public partial class Employee
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Employee()
        {
            PolicyRequests = new HashSet<PolicyRequest>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(250)]
        [DisplayName("Designation:")]
        public string designation { get; set; }

        [Column(TypeName = "date")]
        
        [DisplayName("Join Date:")]
        public DateTime joindate { get; set; }
        [DisplayName("Salary:")]
        public decimal? salary { get; set; }
        [DisplayName("First Name:")]
        [StringLength(250)]
        public string firstname { get; set; }
        [DisplayName("Last Name:")]
        [StringLength(250)]
        public string lastname { get; set; }
        [DisplayName("User Name:")]
        [StringLength(250)]
        public string username { get; set; }
        [DisplayName("Password:")]
        [StringLength(250)]
        public string password { get; set; }
        [DisplayName("Address:")]
        [StringLength(250)]
        public string address { get; set; }
        [StringLength(50)]
        [DisplayName("Contact No:")]
        public string contactno { get; set; }
        [StringLength(50)]
        [DisplayName("State:")]
        public string state { get; set; }
        [StringLength(250)]
        [DisplayName("Country:")]
        public string country { get; set; }
        [StringLength(250)]
        [DisplayName("City:")]
        public string city { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PolicyRequest> PolicyRequests { get; set; }
    }
}
